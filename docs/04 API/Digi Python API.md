# Setting API

The digi-xbee Python API is not a digi product. It is an interface create by contributors as a github project. pypip is updated with the newest stable version.

## Setting up python

Inside your virtual environment install digi-xbee using:

* pip install digi-xbee

For director installation the following can be used:


* python3.8 -m pip install --target="/YourDirectory/virtual/bin" digi-xbee  --upgrade

Note: It is recommended to use virtual environment also in the RPi

## Testing API

Successful installation will allow the following comment.

* from digi.xbee.devices import XBeeDevice

Note: Remember to start python3.8 before running the command.


# Setting the Devices

Xbee devices for the test network

Coordinator: 0013A20041A26F8C
End Point: 0013A20041A26FC4

## for 802.15.4 mode

* Load the firmware
* Make API mode - API1 or API2 enable
* Configure PAN ID - ID Parameter
* Configure Channel ID - CH Parameter
* Configure Node Identifier - NI Parameter - in this case EP for end point and CO for coordinator


## for Zigbee mode

TBA


# Documentation

The official documentation is found here:

https://xbplib.readthedocs.io/en/latest/






