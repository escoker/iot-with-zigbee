# Node Discovery

There are two way to discover a network standart and deep

* standard does not look into sleeping devices
* deep is making an indepth exploration

Furthermore there the following ways of discovery

1. Using specific NODE ID
2. Using specific HEX aka DH and DL/ MAC
3. Swiping the PAN ID without a specific target


## Specific Node: NODE_ID Discovery

		from digi.xbee.device import XBeeDevice
		
		local_device = XBeeDevice(...)
		local_device.open()
		
		xnet = device.get_network()
		remote_device = xnet.discover_device("NODE_ID")
		
		...
		...
		...
		
		local_device.close()



## Specific Node: 64 BIT HEX Discovery


		from digi.xbee.device import XBeeDevice
		from digi.xbee.device import RemoteXBeeDevice
		digi.xbee.device import XBee64BitAddress
		
		local_device = XBeeDevice(...)
		local_device.open()
		x64bit_add = XBee64BitAddress.from_hex_string("MAC_ADDRESS")
		remote_device = RemoteXBeeDevice(local_device, x64bit_add)
		
		
		...
		...
		
		local_device.close()
		
		

## Non Specific Node Discovery

		
		from digi.xbee.device import XBeeDevice
		import time
		
		local_device = XBeeDevice(...)
		local_device.open()
		
		xnet = device.get_network()
		xnet.start_discovery_process()
		
		while xnet.is_discovery_running():
			time.sleep(0.1) #The value is up to the user recommendation is 0.5s
			
		...
		...
		
		local_device.close()
		
		
