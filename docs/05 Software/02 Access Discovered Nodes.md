# Node Access

There are two ways to access a Node with, NODE ID or with the 64BitAddress



## Node ID Access

		from digi.xbee.device import XBeeDevice
		...
		...
		
		xnet = device.get_network()
		
		...
		Network Nodes Discovery
		...
		
		
		spc_node_id = xnet.get_device_by_node_id("NODE_ID")

		
		...
		...
		...
		



## 64 BIT HEX Discovery


		from digi.xbee.device import XBeeDevice
		from digi.xbee.device import RemoteXBeeDevice
		digi.xbee.device import XBee64BitAddress
		
		...
		...
		...
		
		xnet = device.get_network()
		
		...
		Network Nodes Discovery
		...
		
		
		x64Bit = XBee64BitAddress.from_hex_string("MAC")
		
		specific_node = xnet.get_device_by(x64Bit)
		
		
		...
		...
		...
		
		
