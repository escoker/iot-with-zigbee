# General Settings

Using the Python API to control and discover network nodes can create issue of discovery and naming.


## General Xbee Settings

Leave the setting as need but three parameters need to be set up.

* SH = 0
* SL = 0
* MY = 0xFFFE

see [Unicast Transmittion](https://www.digi.com/resources/documentation/Digidocs/90001942-13/concepts/c_transmission_methods_zigbee.htm?TocPath=Wireless%20data%20transmission%7C_____1)
	


## Test project

A test project is been worked on. The .py files will be made available as the project develops.
