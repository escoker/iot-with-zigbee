# Simple Test Software

The software below is for simple testing only and it is missing explanations and steps.
Discovery is done only for one spefic node.


## Simple digi-xbee and network test software


Device Setup

	from digi.xbee.devices import XBeeDevices

	xbee = XBeeDevice("MySerialPort", MyBDRate)

	xbee.open()

Get Network


	xnet = xbee.get_network()

	remote_device = xnet.discover_device("MyRemoteNodeIdentifier")

	device.send(remote_device,"MyDataToSend")

	xbee.close()
	
	


## Simple Test Send

       #!/usr/bin/env python3.7
       from digi.xbee.devices import XBeeDevice
       from digi.xbee.devices import XBee64BitAddress
       from digi.xbee.devices import RemoteXBeeDevice

       def main():
        print("XBee Sending API Frame Data Sample")
        device = XBeeDevice("/dev/ttyUSB0", 9600)
        device.open()

        try:
          xnet = device.get_network()
          remote_device = xnet.discover_device("CO")
 
          print("sending data to %s >> %s" % (remote_device.get_64bit_addr(),"CO"))
 
          device.send_data(remote_device,"Test Transmission")
          print("success \n")
 
          x64bit = XBee64BitAddress.from_hex_string("0013A20041A26F8C")
          spc_node = xnet.get_device_by_64(x64bit)
          print("Node ID is %s \n \n" % str(spc_node))
          print("x64Bit is: %s \n \n" % str(x64bit))
          device.send_data(spc_node,"This is on x64bit")
  
          remote = RemoteXBeeDevice(device, XBee64BitAddress.from_hex_string("0013A20041A26F8C"))
          device.send_data(remote, "This is a hex Test")
 
        finally:
          device.close()


        if __name__ == '__main__':
          main()
          


## Simple Test Receive


      #!/usr/bin/env python3.8
	  from digi.xbee.devices import XBeeDevice
	  # TODO: Replace with the serial port where your local module is connected to.
	  PORT = "/dev/ttyACM3"
	  # TODO: Replace with the baud rate of your local module.
	  BAUD_RATE = 9600


	  def main():

 		  device = XBeeDevice(PORT, BAUD_RATE)
 		  try:
        device.open()
   		  device.add_data_received_callback(data_receive_callback)

			  print("Waiting for data...\n")

 		  finally:
   		  if device is not None and device.is_open():
      	  device.close()


	  def data_receive_callback(xbee_message):
      print("Message: ", xbee_message.data.decode())


    if __name__ == '__main__':
 		  main()


