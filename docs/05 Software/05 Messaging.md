# Simple Test Software

Send and receive examples


## Simple digi-xbee and network test software
		
		software from earlier
		------
		[...]
		Device Setup
		[...]
		Get Network
		
		-----
		your software solution


	


## Sending messages

Using a simple class solution for sending:

* remote_x64bits_address - the 64bit address of destination
* data_to_send - the data package
* local_device - the UART device/PORT for which we have open communication using the .open()


		from digi.xbee.devices import RemoteXBeeDevice

		class xbee_send():

    			def __init__(self, remote_x64bits_address, data_to_send, local_device):
        			self.remote_x64bits_address = remote_x64bits_address
        			self.data_to_send = data_to_send
        			self.local_device = local_device
        			self.send()


    			def send(self):
			        self.remote_device = RemoteXBeeDevice(self.local_device, self.remote_x64bits_address)
        			self.local_device.send_data(self.remote_device, self.data_to_send)
          


## Receive message specific node
A simple send to a specific node. This solution is good for testing and p2p. But the limistations are that each time you change the address the software needs to be reloaded.

		[...]
		remote_device = RemoteXBeeDevice(device, XBee64BitAddress.from_hex_string("0013A20041A26FC4"))
		xbee_msg = device.read_data_from(remote_device)#error in the text - example
		if xbee_msg != None:
               	print("remote 64: ", xbee_msg.remote_device.get_64bit_addr())
               	print("data: ", xbee_msg.data.decode())
               	
              
              
              
## Receive message general listening/pooling

A simple solution for listenting to any message. A good solution but general messaging but time is spend on the listening
		[...]
		
		xbee_msg = device.read_data()
		if xbee_msg != None:
		 print("remote: ", xbee_msg.remote_device)
		 print("remote 64", xbee_msg.remote_device.get_64bit_addr())
		 print("data :", xbee_msg.data.decode())
		 
		 [...]
		 
## Receive message general listening/pooling with callback and subscription
A general class representation:
MY parameters needs to be writen as FFFF or FFFE

* local_device - is the UART .open() devices
* data_retrieve() - method that is called outside the class to retrive data, used as a public/interface

		from digi.xbee.devices import XBeeDevice
		from digi.xbee.devices import RemoteXBeeDevice
		import time
		'''
		MY needs to be set up to FFFF disable 16bit msg or FFFE
		'''
		class receive():

			def __init__(self, local_device):
				self.local_device = local_device
				self.data_received_list = [] #[0]: device [1]: data


			def data_retrieve(self):
        
				self.temp_buffer = self.data_received_list
				self.data_received_list = []
        
				return self.temp_buffer
  
			def receive(self):
				self.local_device.add_data_received_callback(self.data_receive_callback)
				time.sleep(1)
				self.local_device.del_data_received_callback(self.data_receive_callback)


			def data_receive_callback(self, xbee_message):
				self.data_received_list.append(str(xbee_message.remote_device.get_64bit_addr()))
				self.data_received_list.append(xbee_message.data.decode('ascii'))


