# XTCU General

First step is to download the drives if using the Parallax module, for the Waveshare drivers are not needed. Linux newest kernel has all the drivers preinstalled. Plug in the adapter in to the USB and then check under device manager if there is a new device. Under Linux use lsusb to detect the device.

Instructions to the xbee s2c can be send using the AT Commands or as in this case using the XCTU

* Load the newest firmware for 802.15.4
* Or load the newest firmware for Zigbee

## Coordinator without API


### Coordinator on 802.15.4

Set up the following parameters:

* CH Channel - use hex starting from B ending with 1A. This corresponds to the zigbee channels.
* ID PAN ID - ID of the network 0xFFFF is used to send to all PANs on the same channel
* DL Destination Address Low - used for receiving address - set this if you are using p2p connection
* My 16-bit Source Address - This is the Coordinator Address used to connect to the Coordinator
* CE Coordinator Enable - Set to Coordinator [1]

Verify the Serial Interface settings and make changed if needed.

### Coordinator on Zigbee

To be updated with Zigbee firmware is used

## Router

To be updated with Zigbee firmware is used

## End Point on 802.15.4 without API

* CH Channel - Same as Coordinator
* ID PAN ID - Same as Coordinator
* DL Destination Address Low - used for receiving address - set this if you are using p2p connection
* My 16-bit Source Address - This is the End Point Address used to connect to the Device
* CE Coordinator Enable - Set to End Point [0]

Verify the Serial Interface settings and make changed if needed.


## Coordinator on 802.15.4 with API


* CH Channel - use hex starting from B ending with 1A. This corresponds to the zigbee channels.
* ID PAN ID - ID of the network 0xFFFF is used to send to all PANs on the same channel
* DL and DH are the same as SH and SL aka the MAC
* MY is 0
* NI is set to "NODE_ID" for the device - without " " avoid whitespaces
* AP is set to API 1 or AP2

## End Point on 802.15.4 with API

* CH Channel - Same as Coordinator
* ID PAN ID - Same as Coordinator
* DL and DH are the same as SH and SL aka the MAC
* MY is 0
* NI is set to "NODE_ID" for the device - without " " avoid whitespaces
* AP is set to API 1 or AP2


## Encryption

Encryption is important by the examples are without.


## Serial Connection

When the devices are set up the Serial Connection tool can be used. Connect the Coordinator first and then the End Point try sending message between the two.


## Frame Generator

Using the Frame Generator a test can be done between the modules in API mode. Also the Frame can be analyzed and testing done.


## Network Analyzer

This tool can be used to find the optimal channel in the area of the network. Select a network without too much noise and the lowest average traffic.
