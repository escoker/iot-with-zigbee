# IoT with Zigbee

This document will be used to track the development of zigbee iot based network. Further more it will use as a starting point for developing UCLs IoT network.


## General introduction to Digi's Xbee mobule

The module currently in use is the Xbee s2c. With documented range of 60m indoor and 1200m outdoor. It operates at 2.1v up to 3.6v DC. Communication is done using Rx/Tx - UART is also possible.

## XCTU

We are using the XCTU v6 software from Digi for setting up the devices.

## Network

Two types are used 802.15.4 and Zigbee

## Software

We will be using python and the API provided from Digi.


## Hardware

Xbee s2c Modules are used



