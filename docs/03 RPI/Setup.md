# RPi
All the following are done on the RPI

* sudo gedit /boot/config.txt

* check enable_uart0=on : long in via zigbee /dev/serial0 delete this

* check enable_uart=1 : for serial connection

Alternatively use raspi-config so enable serial and or not serial log in

* use dmesg | grep tty

* check ttyS0 settings 

* use ls -l /dev to check for serial0 -> ttyS0

* Disbale the Console - serial log in using the following steps:

    1. sudo systemctl stop serial-getty@ttyS0.service
    2. sudo systemctl disable serial-getty@ttyS0.service

* use sudo gedit /boot/cmdline.txt

* check the console=tty1 value as a second parameter. If it is in the end, move it.
Remove anything that has to do wtih console=serial0 or ttyS0 including BD rate

The following is done on RPI or XTCU

* Connect RX TX of RPI GPIOS to DOU (pin2) and DI (pin3) of the xbee module. 3.3v to Vcc and GND to GND

* Reset all modules xbee coordinator and xbee end point. Pull the power.  If the modules where already connected while setting up Serial on RPI

* Make sure they are on the same network and the coordinator can discover the end point.

* Test with minicom

sudo minicom -s 
minicom -c on

Try to transmitte messages to the RPI from the XTCU

Alternatively try and testing though XCTU the coordinator and the end point


## Information

A simple test program. You will have to develop it further on your own.

### instructions

Use the following test code. Remember to do changes as needed and to make sure that you are following python rules.

        #!/usr/bin/env python3
        import time 
        import serial
        import RPi.GPIO as GPIO


        uart  = serial.Serial("/dev/ttyACM0", baudrate=9600,
                               parity = serial.PARITY_NONE,
                               stopbits=serial.STOPBITS_ONE,
                               bytesize=serial.EIGHTBITS,
                               timeout = 0.050)

        while True:
    
            receiveData += uart.read(10).decode('ascii') #the number are the bits
  
    
            if receiveData == 'q':
                print ("In receiveData")
                uart.write(str.encode('a'))
        
        
# Other Links
1. https://roboticsbackend.com/raspberry-pi-hardware-permissions/
2. https://www.circuits.dk/setup-raspberry-pi-3-gpio-uart/











