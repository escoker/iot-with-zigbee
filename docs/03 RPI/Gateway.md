# Configuration

For Gateway a Linux Ubuntu 20 is used. Gateway can be anything that can host an OS or a strong enough processor with ability to connect to other networks for example TCP/IP.

## Setting up python

* Install Python, in this case Python3.8 is used.
* Set up Virtual Environment - sudo apt install python3-venv
* python3 -m venv yourVirtualEnviroment
 	1. activate with source yourVirtualEnviroment/bin/activate
 	2. use the same path with deactivate to stop the virtual enviroment
* Install pip
 	1. sudo apt install -y python3-pip
* Install the packages you need for example pyserial. Remember to you --upgrade due to everything needs to be installed in the bin of your virtual environment.
 	1. python3.8 -m pip install --target="PATH/yourVirtualEnviroment/bin/" pyserial --upgrade
* You may need to update your pip if you are using an older version
 	1. python3 -m pip install --target="PATH/yourVirtualEnviroment/bin/" --upgrade pip


## Devices

Verify USB adapter as a USB device. Since this is not GPIO installation USB device need to be identify. Useful commands:

* lsusb
* lsblk -a
* dmesag
	 
    ```
	results should be something similar to:
			usb 1-2: new full-speed USB device number 6 using xhci_hcd
			usb 1-2: New USB device found, idVendor=2341, idProduct=0043, bcdDevice= 0.01
			usb 1-2: New USB device strings: Mfr=1, Product=2, SerialNumber=220
			usb 1-2: Manufacturer: Arduino (www.arduino.cc)
			usb 1-2: SerialNumber: 55739323937351207091
			cdc_acm 1-2:1.0: ttyACM0: USB ACM device
	```
* dmesag | grep tty
 	1. results should be something similar to: cdc_acm 1-2:1.0: ttyACM0: USB ACM device
 
The USB device will be used to communicate with the xbee module using python or any other programming languish. In some cases ttyUSB0 can also be seen. This is mostly common when a USB adaptor is used with for example the cp210x chip


## Port Testing

Connected Ports can also be test using the serial libraries command:

python3.8 -m serial.tools.list_ports





