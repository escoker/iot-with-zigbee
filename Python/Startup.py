# "Role.COORDINATOR"
# "Role.END_DEVICE"
# "Role.ROUTER"

from xbee_controller import controller
import threading 
from digi.xbee.devices import XBeeDevice
import test2
import serial.tools.list_ports as ports
import re
import platform
import sys
import time


class initializer():

    def __init__(self):
        self.COORDINATOR = False
        self.END_DEVICE = False
        # self.ROUTER = False #to be added for zigbee network"
        self.graphical = ""
        self.uart = ""
        self.baud_rate = 9600
        self.portlist_clean = []
        self.operting_system()
        self.role_agent()
        self.backcall_agent()

    def operting_system(self):
        system = platform.system()
    
        if system == "Linux":
            self.uart_linux()
        elif system == "Windows":
            self.uart_windows

    def uart_windows(self):
        pass

    def uart_linux(self):
    
        portlist_raw = list(ports.comports())
        if (not portlist_raw):
           print("No UART connection found. Please connect a USB adaptor and try again. Program will terminate")
           sys.exit()
        
        regex = re.compile(".*ttyUSB")
    

        for i in range (0,len(portlist_raw)):
            match = str(portlist_raw[i]).split(' - ')[0]
            if re.match(regex, match):
                self.portlist_clean.append(match)
    
        for i in range(0,len(self.portlist_clean)):
            print(i, " ", self.portlist_clean[i])

        user_input = int(input("select item you would want to initialize: "))
        
       


        print(self.portlist_clean[user_input]) #remove after testing

        self.uart = self.portlist_clean[user_input]
        del self.portlist_clean

    def role_agent(self):

        uart_device = XBeeDevice(self.uart, self.baud_rate)
        uart_device.open()
        if (str(uart_device.get_role()) == "Role.COORDINATOR"):
            self.graphical = input("Do you want a graphical interface or a terminal mode?, please select 0 or 1: ")
            self.COORDINATOR = True 
            uart_device.close()

        elif (str(uart_device.get_role()) == "Role.END_DEVICE"):
            self.END_DEVICE = True
            uart_device.close()

    
    def backcall_agent(self):
        
        if self.COORDINATOR:
            xbee_obj_thread = xbee_thread(self.uart, self.baud_rate)
            xbee_obj_thread.start()
            
            if (self.graphical == "0"):
                graphical_obj_thread = graphical_interface_thread(    )
                graphical_obj_thread.start()
          
            elif(self.graphical == "1"):
                print("Terminal Thread")
            
            

        elif self.END_DEVICE:
            xbee_obj_thread = xbee_thread(self.uart, self.baud_rate)
            xbee_obj_thread.start()

        while True: #main thread infinite loop alt design main thread is graphical or xbee
            print("Main thread")
            time.sleep(0.5)

            #on end conditions I need to join the treads and close



class graphical_interface_thread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        test2.main()

class xbee_thread(threading.Thread):
    def __init__(self, uart, BR):
        self.uart = uart
        self.baud_rate = BR
        threading.Thread.__init__(self)

    def run(self):
        xbee = controller(self.uart, self.baud_rate)
        del xbee


if __name__ == '__main__':

    start = initializer()
    del start