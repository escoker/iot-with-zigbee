from digi.xbee.devices import XBeeDevice
from digi.xbee.devices import RemoteXBeeDevice
import time
'''
MY needs to be set up to FFFF disable 16bit msg or FFFE
'''
class receive_class():

    def __init__(self, local_device):
        self.local_device = local_device
        self.data_received_list = [] #[0]: device [1]: data

    #store data from remote node
    def data_retrieve(self):
        
        temp_buffer = self.data_received_list
        self.data_received_list = []
        return temp_buffer
  
    def receive_data(self):
        self.local_device.add_data_received_callback(self.data_receive_callback)
        time.sleep(1)
        self.local_device.del_data_received_callback(self.data_receive_callback)


    def data_receive_callback(self, xbee_message):
        #I need to clean and split the mac from the node id
        self.data_received_list.append("MAC: " + str(xbee_message.remote_device.get_64bit_addr()))
        self.data_received_list.append("NODE_ID: " + str(xbee_message.remote_device.get_node_id()))
        self.data_received_list.append("DATA: " + xbee_message.data.decode('ascii'))
       

