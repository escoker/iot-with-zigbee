import time
from digi.xbee.devices import RemoteXBeeDevice
from digi.xbee.devices import XBee64BitAddress
from digi.xbee.models.status import NetworkDiscoveryStatus
from digi.xbee.models.options import DiscoveryOptions

class communication():

    
    def __init__(self, uart_device):
        self.local_uart = uart_device
        self.nodes = []
        self.discover()
        #run discover and save nodes and ID in list

    def getter (self):
        return self.nodes

    def discover(self):
        try:

        #runs discover
        #saves nodes and ID
            self.xnetwork = self.local_uart.get_network()
            self.xnetwork.set_discovery_options({DiscoveryOptions.DISCOVER_MYSELF})

            self.xnetwork.set_discovery_timeout(10)

            self.xnetwork.add_device_discovered_callback(self.callback)

            self.xnetwork.add_discovery_process_finished_callback(self.callable_finish)

            self.xnetwork.start_discovery_process()

            while self.xnetwork.is_discovery_running():
                time.sleep(0.5)

        finally:
            print("end of discovery")


       


    def callback(self, remote):
        remote_string = str(remote)
        self.nodes.append("MAC: " + remote_string.split(' - ')[0])
        self.nodes.append("NODE ID: " + remote_string.split(' - ')[1])        


    def callable_finish(self, status):
        if status == NetworkDiscoveryStatus.SUCCESS:
            print("no errors detected")
            #clear the list of nodes
            self.xnetwork.clear()

        else:
            print("something went wrong: %s" % status.description)

            