from digi.xbee.devices import XBeeDevice
import xbee_discovery
from xbee_send import xbee_send_class
from xbee_receive import receive_class
from HMI_file_manip import file_editor


class controller():
    def __init__(self, remote_uart, remote_baud_rate):
        self.device = remote_uart
        self.baud_rate = remote_baud_rate


        self.file_editor = file_editor()
        
        self.device_setup()    
        self.discovery_method()

        self.xbee_sender = xbee_send_class(self.uart_device)
        self.xbee_receiver = receive_class(self.uart_device)

        self.communication()



    def discovery_method(self):

        self.nodes_found = False
        count = 0

        self.discovery_module = xbee_discovery.communication(self.uart_device)
        node_list = self.discovery_module.getter() #no extra cleaning needed        
        del self.discovery_module #remove object from memory. We do not need discovery to remain
        self.file_editor.writer("03_print_test", node_list)
       
        for item in node_list:
            count +=1
        if count > 3:
            self.nodes_found = True

        del node_list
        del count

    def data_sender(self):

        file_output = []
        file_output = self.file_editor.reader("02_print_test")        
       
        if ((file_output) and (self.nodes_found)): # not empty and more than one node found
            self.xbee_sender.interface(str(file_output[0]), str(file_output[1]).strip('\n'))
       # else:
       #     print("No nodes to send to") #this will be pass after testing, on graphical mode print statments have no effect due to blocking

        del file_output

    def data_receiver(self):
        self.xbee_receiver.receive_data()
        local_data = []
        if (self.nodes_found):
            local_data = self.xbee_receiver.data_retrieve()
            self.file_editor.writer("01_print_test", local_data)
        #else:
        #    print("No Nodes to receive from") #this will be pass after testing, on graphical mode print statments have no effect due to blocking
        del local_data

    def communication(self):
        try:
            while True:
                self.data_sender()
                self.data_receiver()
                #need a break and stop. Most likely from UI or xbee command
        finally:
            pass#self.finish()
    
    def device_setup(self):
        self.uart_device = XBeeDevice(self.device, self.baud_rate)
        self.uart_device.open()
        print("uart open") #this will be pass after testing, on graphical mode print statments have no effect due to blocking

    def device_stop(self):
        self.uart_device.close()
        print("device closed") #this will be pass after testing, on graphical mode print statments have no effect due to blocking
        
