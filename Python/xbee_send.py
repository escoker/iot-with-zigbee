from digi.xbee.devices import RemoteXBeeDevice
from digi.xbee.devices import XBee64BitAddress


class xbee_send_class():

    def __init__(self, local_device):
        self.local_device = local_device

    def interface(self, remote_device, data_out):
        
        self.remote_x64bits_address = XBee64BitAddress.from_hex_string(remote_device)
        self.data_to_send = data_out
        self.send()

    def send(self):
        remote_device = RemoteXBeeDevice(self.local_device, self.remote_x64bits_address)
        self.local_device.send_data(remote_device, self.data_to_send)