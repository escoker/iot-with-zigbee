#!/usr/bin/env python3.8

from digi.xbee.devices import XBeeDevice
from digi.xbee.devices import XBee64BitAddress
from digi.xbee.devices import RemoteXBeeDevice
import time

# TODO: Replace with the serial port where your local module is connected to.
PORT = "/dev/ttyUSB2"
# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600


def main():
    print("Main loop")

    device = XBeeDevice(PORT, BAUD_RATE)
    try:
        device.open()
        while True:

            print("in while")

        # specific Node
            remote_device = RemoteXBeeDevice(device, XBee64BitAddress.from_hex_string("0013A20041A26FC4"))
            #print("remote device, ", remote_device.get_64bit_addr())
        #    xbee_msg = device.read_data_from(remote_device)#error in the text - example
        #    if xbee_msg != None:
        #        print("remote 64: ", xbee_msg.remote_device.get_64bit_addr())
        #        print("data: ", xbee_msg.data.decode())

           
        #Listen to msg
           # xbee_msg = device.read_data()
           # if xbee_msg != None:
           #     print("remote: ", xbee_msg.remote_device)
           #     print("remote 64", xbee_msg.remote_device.get_64bit_addr())
           #     print("data :", xbee_msg.data.decode())
          


        # data polling with call back
            device.add_data_received_callback(data_receive_callback)
           # print("Waiting for data...\n")
           # print(device.read_data(remote_device))
            time.sleep(3)
        
        
        #needs callback removal
            device.del_data_received_callback(data_receive_callback)


    finally:
        if device is not None and device.is_open():
            device.close()


def data_receive_callback(xbee_message):
    print("Message: \n", xbee_message.data.decode('ascii'))
   # print("remote device \n", xbee_message.remote_device)
    print("TESTER: ", xbee_message.remote_device.get_64bit_addr())
            



if __name__ == '__main__':
    main()
