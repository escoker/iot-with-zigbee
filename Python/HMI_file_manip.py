from pathlib import Path


class file_editor():
    def __init__(self):
        pass

    def writer(self, filepath, data_to_file):
        try:
            basepath = Path(filepath)
            length = len(data_to_file)
            with open(basepath, 'a') as file: #open with 'w' over writes open with 'a' writes at the end of file
                try:
                    for items in range(length):
                        file.write(data_to_file[items] + "\n")

                finally:
                    file.close()
        finally:
            pass


    def reader(self, filepath):
        output_from_file = []
        try:
            basepath = Path(filepath)
            with open(basepath, 'r') as file:
                try:
                    for line in file:
                        for i in range (0,2):
                            output_from_file.append(line.split(' - ')[i])                                                      
                finally:
                    file.close()
        finally:
            pass
        return output_from_file