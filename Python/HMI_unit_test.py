'''
this is for unit testing. The file manip class will be use by others
'''
from HMI_file_manip import file_editor
import time

class HMI_controller():
    def __init__(self):
        self.file_editor = file_editor()
        self.main()

    def load_to_file(self):
        self.file_editor.writer("/home/rutr/Programming/Teaching/Python/01_print_test", self.data_handler)
        self.data_handler = ""

    def load_from_file(self):
        self.data_handler = self.file_editor.reader("/home/rutr/Programming/Teaching/Python/02_print_test")

    def terminal_interface(self):
        pass


    def main(self):

        while True:
            self.load_from_file()
            self.load_to_file()
            time.sleep(1)
        
        del self.file_editor

def main():
    HMI = HMI_controller()
    del HMI

if __name__ == "__main__":
    main()